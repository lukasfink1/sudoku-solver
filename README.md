# High performance sudoku solver with multithreading support
Please don’t ask; I’m aware that this is pointless.

## Usage
```
usage: ./a.out [OPTION]... [INFILE] [OUTFILE]

Sudoku solver

  INFILE                 read sudoku from INFILE; omit or specify - to read
                         from stdin
  OUTFILE                write solutions to OUTFILE

  -T NUM                 use NUM threads
  -s                     stop after finding the first solution
  -c                     print solutions in compact format
  -o                     print solutions in oneline format
  -h                     print this help

When reading in a sudoku, 1-9 stands for the corresponding number and . for
an empty field. All other characters are ignored.
```

You probably want to specify more threads than you actually have cores, because sometimes threads are idling. E. g. I found that 16 threads worked best on my quad core cpu. Not that it really matters, because it’ll probably finish in under 0.0001 s anyway if you give it a regular sudoku.
