CC = gcc
LD = gcc
RM = rm
EXECUTABLE = sudoku-solver
LIBS = -pthread
OBJS = ./sudoku.o
CFLAGS = -march=native -O3 -Wall
LDFLAGS =

.PHONY: all clean

all: $(EXECUTABLE)

clean:
	$(RM) $(EXECUTABLE) $(OBJS)

$(EXECUTABLE): $(OBJS)
	@echo 'Building target $@'
	$(LD) $(LDFLAGS) -o "$@" $(LIBS) $(OBJS)

%.o: src/%.c
	@echo 'Building file $<'
	$(CC) -c $(CFLAGS) -o "$@" "$<"
