/*
 * Copyright (c) 2022 Lukas Fink
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <stdint.h>
#if !defined DISABLE_THREADS
#	include <pthread.h>
#endif

#define ENTRY_MAX 8
#define FIELD_HEIGHT (ENTRY_MAX + 1)
#define FIELD_WIDTH (ENTRY_MAX + 1)
#define CANDIDATES_MASK ((1 << (ENTRY_MAX + 1)) - 1)

#define EMPTY_CHAR '.'
#define MIN_CHAR '1'
#define PRETTY_PRINT_H_DELIM '|'
#define PRETTY_PRINT_V_DELIM "-------|-------|-------"

#ifdef DISABLE_THREADS
#	define ARGSTR "hcos:"
#else
#	define ARGSTR "hcos:T:"
#endif

#if defined __has_builtin && __has_builtin(__builtin_popcount)
#	define popcount9(entry) __builtin_popcount(entry)
#else
#	define popcount9(entry) \
	({ \
		uint32_t tmp = (entry) - (((entry) >> 1) & 0x55555555); \
		tmp = (tmp & 0x33333333) + ((tmp >> 2) & 0x33333333); \
		(((tmp + (tmp >> 4)) & 0xF0F0F0F) * 0x1010101) >> 24; \
	})
#endif

#if defined __has_builtin
#	if __has_builtin(__builtin_ctz)
#		define ctz9(x) __builtin_ctz(x)
#	elif __has_builtin(__builtin_ffs)
#		define ctz9(x) (__builtin_ffs(x) - 1)
#	endif
#elif defined __has_include && __has_include(<strings.h>)
#	include <strings.h>
#	define ctz9(x) (ffs(x) - 1)
#else
#	define ctz9(x) \
	({ \
		static const unsigned char lut[] = { 5, 0, 1, 0, 2, 0, 1, 0, 3, 0, 1, 0, 2, 0, 1, 0, \
		                                     4, 0, 1, 0, 2, 0, 1, 0, 3, 0, 1, 0, 2, 0, 1, 0 }; \
		(x & 0x1f) ? lut[x & 0x1f] : lut[(x >> 5) & 0x1f] + 5; \
	})
#endif

#if defined __has_builtin && __has_builtin(__builtin_expect)
#	define likely(x)   __builtin_expect(!!(x), 1)
#	define unlikely(x) __builtin_expect(!!(x), 0)
#else
#	define likely(x)   (!!(x))
#	define unlikely(x) (!!(x))
#endif

enum check_mode
{
	CHECK_MODE_BLOCK,
	CHECK_MODE_VERTICAL,
	CHECK_MODE_HORIZONTAL,
	CHECK_MODE_LAST
};

enum output_mode
{
	OUTPUT_MODE_PRETTY,
	OUTPUT_MODE_COMPACT,
	OUTPUT_MODE_ONELINE,
	OUTPUT_MODE_LAST
};

enum eliminate_candidates_state
{
	ELIMINATE_CANDIDATES_OK,
	ELIMINATE_CANDIDATES_INVALID,
	ELIMINATE_CANDIDATES_UNSOLVEABLE
};

typedef int_least16_t entry_t;
typedef unsigned char value_t;
typedef unsigned char coord_t;

struct frame_obj
{
#if !defined DISABLE_THREADS
	char recurse_lvl;
#endif
	entry_t field[FIELD_HEIGHT][FIELD_WIDTH];
};

struct global_obj
{
	const char *program_name;
	FILE *out;
#if !defined DISABLE_THREADS
	unsigned long working_threads;
	pthread_mutex_t output_mutex;
	pthread_mutex_t work_mutex;
	pthread_cond_t work_null;
	pthread_cond_t assigned_work_true;
	struct frame_obj *work;
	char assigned_work;
#endif
	char output_mode;
	char stop_after_first;
	char solved;
};

extern char *optarg;
extern int optind;

static void usage(const char *name)
{
	printf("usage: %s [OPTION]... [INFILE] [OUTFILE]\n\n"
		"Sudoku solver\n\n"
		"  INFILE                 read sudoku from INFILE; omit or specify - to read\n"
		"                         from stdin\n"
		"  OUTFILE                write solutions to OUTFILE\n\n"
#if !defined DISABLE_THREADS
		"  -T NUM                 use NUM threads\n"
#endif
		"  -s                     stop after finding the first solution\n"
		"  -c                     print solutions in compact format\n"
		"  -o                     print solutions in oneline format\n"
		"  -h                     print this help\n\n"
		"When reading in a sudoku, 1-9 stands for the corresponding number and . for\n"
		"an empty field. All other characters are ignored.\n", name);
	exit(0);
}

#ifdef DISABLE_THREADS
static void argparse(int argc, char *argv[], char *output_mode, char *stop_after_first)
#else
static void argparse(int argc, char *argv[], char *output_mode, char *stop_after_first, unsigned long *threads)
#endif
{
	char c;
#if !defined DISABLE_THREADS
	char *end = NULL;
#endif

	while ((c = getopt(argc, argv, ARGSTR)) != -1) {
		switch (c) {
		case 'h':
			usage(argv[0]);
			break;
		case 'c':
			*output_mode = OUTPUT_MODE_COMPACT;
			break;
		case 'o':
			*output_mode = OUTPUT_MODE_ONELINE;
			break;
		case 's':
			*stop_after_first = 1;
			break;
#if !defined DISABLE_THREADS
		case 'T':
			*threads = strtoul(optarg, &end, 0);
			if (unlikely(*end || *threads < 1)) {
				fprintf(stderr, "%s: invalid number of threads: %s\n", argv[0], optarg);
				exit(2);
			}
			break;
#endif
		case '?':
			fprintf(stderr, "Try '%s -h' for more information.\n", argv[0]);
			exit(2);
			break;
		}
	}

	if (unlikely(argc - optind > 2)) {
		fprintf(stderr, "%s: extra operand: %s\n", argv[0], argv[argc]);
		exit(2);
	}
}

static entry_t *read_field(entry_t field[], FILE *stream)
{
	int i, c;

	for (i = 0; i < FIELD_HEIGHT * FIELD_WIDTH; i++) {
		do {
			c = fgetc(stream);
			switch (c) {
			case EOF: return NULL;
			case EMPTY_CHAR:
				field[i] = -1;
				goto next_entry;
			}
		} while (c < MIN_CHAR || c > MIN_CHAR + ENTRY_MAX);

		field[i] = c - MIN_CHAR;
	next_entry: ;
	}

	return field;
}

static int print_field(const entry_t field[][FIELD_WIDTH], char mode, FILE *stream)
{
	coord_t i, j;
	char buf[FIELD_HEIGHT * (2 * (FIELD_WIDTH + (FIELD_WIDTH - 1) / 3) + 1)
		+ ((FIELD_HEIGHT - 1) / 3) * (sizeof (PRETTY_PRINT_V_DELIM "\n") - 1) + 1];
	char *pos = buf;

	for (i = 0; i < FIELD_HEIGHT; i++) {
		if (mode == OUTPUT_MODE_PRETTY && unlikely(i != 0 && !(i % 3))) {
			strcpy(pos, PRETTY_PRINT_V_DELIM "\n");
			pos += sizeof (PRETTY_PRINT_V_DELIM "\n") - 1;
		}

		for (j = 0; j < FIELD_WIDTH; j++) {
			if (mode == OUTPUT_MODE_PRETTY) {
				*pos++ = ' ';

				if (unlikely(j != 0 && !(j % 3))) {
					*pos++ = PRETTY_PRINT_H_DELIM;
					*pos++ = ' ';
				}
			}

			*pos++ = (field[i][j] >= 0) ? field[i][j] + MIN_CHAR : EMPTY_CHAR;
		}
		if (mode != OUTPUT_MODE_ONELINE)
			*pos++ = '\n';
	}
	*pos++ = '\n';

	return !fwrite(buf, pos - buf, 1, stream);
}

static int eliminate_candidates(entry_t field[][FIELD_WIDTH])
{
	coord_t i, j, base_x = 0, base_y = 0;
	char mode, write;
	entry_t *entry = NULL, mask;

	for (mode = 0; mode < CHECK_MODE_LAST; mode++) {
		for (i = 0; i <= ENTRY_MAX; i++) {
			mask = -1;
			if (mode == CHECK_MODE_BLOCK) {
				base_y = 3 * (i / 3);
				base_x = 3 * (i % 3);
			}
			for (write = 0; write <= 1; write++) {
				for (j = 0; j <= ENTRY_MAX; j++) {
					switch (mode) {
					case CHECK_MODE_BLOCK:
						entry = &field[base_y + j / 3][base_x + j % 3];
						break;
					case CHECK_MODE_VERTICAL:
						entry = &field[j][i];
						break;
					case CHECK_MODE_HORIZONTAL:
						entry = &field[i][j];
						break;
					}
					if (!write && *entry >= 0) {
						if (unlikely(!(mask & (1 << *entry))))
							return ELIMINATE_CANDIDATES_INVALID;
						mask &= ~(1 << *entry);
					} else if (write && *entry < 0) {
						*entry &= mask;
						if (unlikely(!(*entry & CANDIDATES_MASK)))
							return ELIMINATE_CANDIDATES_UNSOLVEABLE;
					}
				}
				if (unlikely(!(write || mask & CANDIDATES_MASK)))
					break;
			}
		}
	}

	return ELIMINATE_CANDIDATES_OK;
}

static value_t find_min(const entry_t field[][FIELD_WIDTH], coord_t *y, coord_t *x)
{
	coord_t i, j;
	entry_t entry;
	value_t count, min = ENTRY_MAX + 2;

	for (i = 0; i < FIELD_HEIGHT; i++) {
		for (j = 0; j < FIELD_WIDTH; j++) {
			entry = field[i][j];
			if (likely(entry >= 0))
				continue;

			count = popcount9(entry & CANDIDATES_MASK);

			if (unlikely(count >= min))
				continue;
			min = count;
			*y = i;
			*x = j;

			if (min <= 1)
				return min;
		}
	}

	return min;
}

static int update_field(entry_t field[][FIELD_WIDTH], coord_t y, coord_t x, entry_t val)
{
	coord_t i, base_x = x - x % 3, base_y = y - y % 3;
	char mode;
	entry_t *entry = NULL, mask = ~(1 << val);

	field[y][x] = val;

	for (mode = 0; mode < CHECK_MODE_LAST; mode++) {
		for (i = 0; i <= ENTRY_MAX; i++) {
			switch (mode) {
			case CHECK_MODE_BLOCK:
				entry = &field[base_y + i / 3][base_x + i % 3];
				break;
			case CHECK_MODE_VERTICAL:
				entry = &field[i][x];
				break;
			case CHECK_MODE_HORIZONTAL:
				entry = &field[y][i];
				break;
			}
			if (*entry < 0) {
				*entry &= mask;
				if (!(*entry & CANDIDATES_MASK))
					return -1;
			}
		}
	}

	return 0;
}

static void solve(struct frame_obj *fr, struct global_obj *gl)
{
	struct frame_obj fr2;
	entry_t entry;
	value_t cur_cand;
	coord_t x, y;

tail_call:

	if (unlikely(find_min(fr->field, &y, &x) > ENTRY_MAX + 1)) {
#if !defined DISABLE_THREADS
		if (unlikely(gl->stop_after_first))
			pthread_mutex_lock(&gl->output_mutex);
#endif
		if (unlikely(print_field(fr->field, gl->output_mode, gl->out))) {
			perror(gl->program_name);
			exit(126);
		}
		if (unlikely(gl->stop_after_first))
			exit(0);
		gl->solved = 1;
		return;
	}

	entry = fr->field[y][x] & CANDIDATES_MASK;
	goto while_cond;
	do {
		memcpy(fr2.field, fr->field, sizeof (fr->field));
		if (likely(update_field(fr2.field, y, x, cur_cand)))
			continue;

#if !defined DISABLE_THREADS
		fr2.recurse_lvl = fr->recurse_lvl + 1;

		if (unlikely(fr->recurse_lvl < 40 && !gl->assigned_work)) {
			pthread_mutex_lock(&gl->work_mutex);
			if (likely(!gl->assigned_work)) {
				*gl->work = fr2;
				gl->assigned_work = 1;
				pthread_cond_signal(&gl->assigned_work_true);
				pthread_mutex_unlock(&gl->work_mutex);
				continue;
			}
			pthread_mutex_unlock(&gl->work_mutex);
		}
#endif

		solve(&fr2, gl);

while_cond:
		cur_cand = ctz9(entry);
		entry &= ~(1 << cur_cand);
	} while (entry);

	if (unlikely(!update_field(fr->field, y, x, cur_cand))) {
#if !defined DISABLE_THREADS
		fr->recurse_lvl += 1;
#endif
		goto tail_call;
	}
}

#if !defined DISABLE_THREADS
static void *worker(void *arg)
{
	struct global_obj *gl = arg;
	struct frame_obj fr;

	while (1) {
		pthread_mutex_lock(&gl->work_mutex);
		gl->working_threads--;
		if (unlikely(!gl->working_threads && !gl->assigned_work))
			goto end;

		while (gl->work)
			pthread_cond_wait(&gl->work_null, &gl->work_mutex);

		if (unlikely(!gl->working_threads))
			goto end;

		gl->work = &fr;
		gl->assigned_work = 0;
		while (!gl->assigned_work)
			pthread_cond_wait(&gl->assigned_work_true, &gl->work_mutex);

		gl->work = NULL;
		pthread_cond_signal(&gl->work_null);

		gl->working_threads++;
		pthread_mutex_unlock(&gl->work_mutex);

		solve(&fr, gl);
	}

end:
	exit(!gl->solved);
	return NULL;
}
#endif

int main(int argc, char *argv[])
{
	FILE *in = stdin;
	struct global_obj gl = {
		argv[0],
		stdout,
#if !defined DISABLE_THREADS
		1,
		PTHREAD_MUTEX_INITIALIZER,
		PTHREAD_MUTEX_INITIALIZER,
		PTHREAD_COND_INITIALIZER,
		PTHREAD_COND_INITIALIZER,
		NULL,
		1,
#endif
		OUTPUT_MODE_PRETTY,
		0,
		0
	};
	struct frame_obj fr;
#if !defined DISABLE_THREADS
	unsigned long i;
	pthread_t thread;
#endif

#ifdef DISABLE_THREADS
	argparse(argc, argv, &gl.output_mode, &gl.stop_after_first);
#else
	argparse(argc, argv, &gl.output_mode, &gl.stop_after_first, &gl.working_threads);
#endif

	switch (argc - optind) {
	case 2:
		if (strcmp(argv[optind + 1], "-")
			&& unlikely((gl.out = fopen(argv[optind + 1], "w")) == NULL))
			goto err;
		/* fallthrough */
	case 1:
		if (strcmp(argv[optind], "-")
			&& unlikely((in = fopen(argv[optind], "r")) == NULL))
			goto err;
	}

	if (unlikely(read_field(fr.field[0], in) == NULL)) {
		if (feof(in)) {
			fprintf(stderr, "%s: unexpected eof\n", argv[0]);
			return 4;
		} else {
			goto err;
		}
	}
	if (in != stdin)
		fclose(in);
	in = NULL;

	switch (eliminate_candidates(fr.field)) {
	case ELIMINATE_CANDIDATES_INVALID:
		fprintf(stderr, "%s: invalid sudoku\n", argv[0]);
		return 3;
	case ELIMINATE_CANDIDATES_UNSOLVEABLE:
		return 1;
	}
#if !defined DISABLE_THREADS
	fr.recurse_lvl = 0;

	for (i = gl.working_threads; i > 1 ; i--)
		pthread_create(&thread, NULL, worker, &gl);
#endif

	solve(&fr, &gl);
#if !defined DISABLE_THREADS
	worker(&gl);
#endif
	return !gl.solved;

err:
	perror(argv[0]);
	return 127;
}
